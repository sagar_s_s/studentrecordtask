public with sharing class studentRecordHandler {

    @AuraEnabled
    public static List<fieldWrapper> getFieldSetMembers(String objectApiName, String fieldSetApiName)
    {
        List<fieldWrapper> lstfieldWrapper = new List<fieldWrapper>();
        Schema.DescribeSObjectResult d = Student__c.sObjectType.getDescribe();

        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
    Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectApiName);
    Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();

    //system.debug('====>' + DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName));

    Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetApiName);

    List<Schema.FieldSetMember> fieldSetMemberList =  fieldSetObj.getFields();
    for(Schema.FieldSetMember objFM : fieldSetMemberList) 
    {
        lstfieldWrapper.add(new fieldWrapper(objFM.getLabel(), objFM.getFieldPath(), objFM.getRequired(), String.valueOf(objFM.getType()), objFM.getDbRequired()));
    }
    //system.debug('fieldSetMemberList ====>' + fieldSetMemberList);  
    return lstfieldWrapper; 

    }


    @AuraEnabled
    public static Object createAccount(Student__c objStudent){
        try {
            if (String.isNotBlank(objStudent.Id)) 
            {
            update objStudent;
            return new Map<String,string> {'Id' => objStudent.Id, 'Name' => objStudent.Name};
        }else if (String.isNotBlank(objStudent.Name))
        {
            insert objStudent;
            return objStudent;
        }
        return objStudent;
       
    } catch (Exception e) {
        throw new AuraHandledException(e.getMessage());
    }
}


    public class fieldWrapper 
    {
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public String apiName {get; set;}
        @AuraEnabled public Boolean isRequired {get; set;}
        @AuraEnabled public String type {get; set;}
        @AuraEnabled public String isDBRequired {get; set;}

        fieldWrapper(String label, String apiName, Boolean isRequired, String type, Boolean isDBRequried) 
        {
            this.label = label;
            this.apiName = apiName;
            this.isRequired = isRequired;
            this.type = type;
            this.isDBRequired = isDbRequired; 
        }
    }


    @AuraEnabled
    public static List<Student__c> getDetails(String recordId){
        try {
            String query = 'SELECT ';
            for(fieldWrapper objFields : getFieldSetMembers('Student__c', 'studentFieldsForLWC')) 
            {
                query += objFields.apiName+ ',';
            }

            String sId = '\''+recordId+'\'';
            query += ' Id FROM Student__c WHERE ID ='+sId+'';
            List< Student__c> lstStudent = Database.query(query);   
            System.debug('query ===>'+query );
            return lstStudent;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

}