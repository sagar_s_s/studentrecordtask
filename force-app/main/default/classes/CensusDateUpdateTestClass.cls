@isTest public class CensusDateUpdateTestClass 
{
    @isTest public static void onBeforeInsertTwo(){
        
        Account objAcc = new Account( Name ='TestAccount');
        insert objAcc;
        Census__c objCen = new Census__c ( Name ='Census01' ,vlocity_ins_GroupId__c = objAcc.Id,vlocity_ins_EffectiveStartDate__c = Date.newInstance(2022, 12, 29),vlocity_ins_EffectiveEndDate__c = Date.newInstance(2021, 12, 31));
        insert objCen;
        
        Census__c objCenNew = [SELECT Id, Name, vlocity_ins_GroupId__c,vlocity_ins_EffectiveStartDate__c,vlocity_ins_EffectiveEndDate__c FROM Census__c];
        system.debug('objCenNew' +objCenNew);
        Census_Member__c objCm = new Census_Member__c(); 
        objCm.Name = 'Silverline'; 
        objCm.vlocity_ins_CensusId__c = objCenNew.Id;
        //objCm.htcs_EffectiveDate__c = Date.newInstance(2021, 12, 29);
        //objCm.htcs_TermDate__c = Date.newInstance(2021, 12, 31);
        Test.startTest();
        try{
            objCm.htcs_EffectiveDate__c = Date.newInstance(2022, 12, 29);
            objCm.htcs_TermDate__c = Date.newInstance(2022, 12, 31);
            insert objCm;
        }
        catch(Exception e){
            String message = e.getMessage();
            System.assert(message.contains('Invalid Dates,Plz enter correct dates'),'message=' +message);  
        } 
        Test.stopTest();
    }
    @isTest public static void onBeforeUpdateTwo() {
        Account objAcc = new Account( Name ='TestAccount');
        insert objAcc;
        Census__c objCen = new Census__c (Name ='Census01' ,vlocity_ins_GroupId__c = objAcc.Id,vlocity_ins_EffectiveStartDate__c = Date.newInstance(2021, 12, 29),vlocity_ins_EffectiveEndDate__c = Date.newInstance(2021, 12, 31));
        insert objCen;
        Census__c objjCenNew = [SELECT Id, Name, vlocity_ins_GroupId__c,vlocity_ins_EffectiveStartDate__c,vlocity_ins_EffectiveEndDate__c FROM Census__c];
        system.debug('objjCenNew' +objjCenNew);
        Census_Member__c objCenmem = new Census_Member__c(); 
        objCenmem.Name = 'Silverlinecrm';
        objCenmem.htcs_EffectiveDate__c = Date.newInstance(2021, 12, 29);
        objCenmem.htcs_TermDate__c = Date.newInstance(2021, 12, 31);
        objCenmem.vlocity_ins_CensusId__c = objjCenNew.Id;
        insert objCenmem;
        //objCenmem.htcs_EffectiveDate__c = Date.newInstance(2022, 12, 29);
        //objCenmem.htcs_TermDate__c = Date.newInstance(2022, 12, 31);
        Test.startTest();
        try {
            objCenmem.htcs_EffectiveDate__c = Date.newInstance(2022, 12, 29);
            objCenmem.htcs_TermDate__c = Date.newInstance(2022, 12, 31);
            update objCenmem;
        }
        catch(Exception e){
            String message = e.getMessage();
            System.assert(message.contains('Check the dates,Enter it in correct way'),'message=' +message);  
        } 
        Test.stopTest();
    }
}