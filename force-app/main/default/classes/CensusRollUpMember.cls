public class CensusRollUpMember 
{ 
    public static void onAfterInsertCensusRollUpMember(List<Census_Member__c> lstCensusMem) 
    {
        List<Census__c> lstCenNames = new List<Census__c>();
        Map<Id, Census__c> mapCensus = new Map<Id, Census__c>([SELECT Id,Name,Members__c FROM Census__c]);
        for(Census_Member__c objMemberCen : lstCensusMem) 
        {
            if(objMemberCen.vlocity_ins_CensusId__c == mapCensus.get(objMemberCen.vlocity_ins_CensusId__c).Id)
            {
                Census__c objCensus = new Census__c();
                objCensus.Id = objMemberCen.vlocity_ins_CensusId__c;
                objCensus.Members__c = mapCensus.get(objMemberCen.vlocity_ins_CensusId__c).Members__c + ' : ' +objMemberCen.Name;
                lstCenNames.add(objCensus);
            }
            
        }
        update lstCenNames;        
    }
    
     public static void onAfterUpdateCensusRollUpMember(List<Census_Member__c> lstCM, Map<Id, Census_Member__c> oldMapOfCM){
        Set<Id> setOfId = new Set<Id>();
        Map<Id, String> mapOfCM = new Map<Id, String>();
        for(Census_Member__c objCenMem : lstCM){
            setOfId.add(objCenMem.vlocity_ins_CensusId__c);
        }
       
        for(Census_Member__c objCM : [SELECT Id, Name, vlocity_ins_CensusId__c FROM Census_Member__c WHERE vlocity_ins_CensusId__c IN : setOfId]){
            if(!mapOfCM.containsKey(objCM.vlocity_ins_CensusId__c)){
                mapOfCM.put(objCM.vlocity_ins_CensusId__c, objCM.Name);
            }
            else{
                 mapOfCM.put(objCM.vlocity_ins_CensusId__c, mapOfCM.get(objCM.vlocity_ins_CensusId__c) + ' ; ' + objCM.Name);
            }
        }
       
        List<Census__c> lstCensus = new List<Census__c>();
        for(Census_Member__c objCenMem : lstCm){
            if(objCenMem.Name != oldMapOfCM.get(objCenMem.Id).Name){
                Census__c objCensus = new Census__c();
                objCensus.Id = objCenMem.vlocity_ins_CensusId__c;

                lstCensus.add(objCensus);
            }
        }
        update lstCensus;
    }

}