@isTest public class JewelryTestClass 
{
    @isTest public static void  testOnBeforeInsert()
    {
        Jewelry__c objJwl = new Jewelry__c(); 
        objJwl.Name = 'Diamond';                                   
        objJwl.Unit_Price__c = 100;                                 
        objJwl.Quantity__c = 2;
        Test.startTest();
        insert objJwl;
        system.debug('objJwl' +objJwl);
        Test.stopTest();
        List<Jewelry__c> lstJwl = [SELECT Name,Unit_Price__c,Quantity__c FROM Jewelry__c];
        System.assertEquals(1,lstJwl.size());
    }
    @isTest public static void  testOnBeforeUpdate()
    {
        Jewelry__c objJwlUpdate = new Jewelry__c();
        objJwlUpdate.Name = 'Gold'; 
        objJwlUpdate.Unit_Price__c = 150;
        objJwlUpdate.Quantity__c = 2;
        insert objJwlUpdate;
        objJwlUpdate.Unit_Price__c = 600 ;
        objJwlUpdate.Quantity__c = 4;
        Test.startTest();
        update objJwlUpdate;
        system.debug('objJwlUpdate' +objJwlUpdate);
        Test.stopTest();
        List<Jewelry__c> lstJwlUpdate = [SELECT Name,Unit_Price__c,Quantity__c FROM Jewelry__c];
        System.assertEquals(1,lstJwlUpdate.size());
    }
}