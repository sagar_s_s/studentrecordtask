@isTest public class CensusPrimaryUpdateTestCalss {
    
    @isTest public static void onBeforeInsertThree(){
        
        //Account objAcc = new Account( Name ='TestAccount');
       // insert objAcc;
        //Census__c objCen = new Census__c ( Name ='Census01' ,vlocity_ins_GroupId__c = objAcc.Id);
       // insert objCen;
        
        Census__c objCenNew = [SELECT Id,vlocity_ins_GroupId__c,Primary_Member__c FROM Census__c ];
        system.debug('objCenNew' +objCenNew);
        Census_Member__c objCm = new Census_Member__c(); 
         Test.startTest();
        try{
            objCm.Name = 'Silverline';
            objCm.vlocity_ins_IsPrimaryMember__c = FALSE; 
            objCm.vlocity_ins_CensusId__c = objCenNew.Id;
            insert objCm;
        }
        catch(Exception e){
            String message = e.getMessage();
            System.assert(message.contains('Primary memebr is present in the census'),'message=' +message);  
        } 
        Test.stopTest();
    }
    @isTest public static void onBeforeUpdateThree(){
        Account objAcc = new Account( Name ='TestAccount');
        insert objAcc;
        Census__c objCen = new Census__c ( Name ='Census01' ,vlocity_ins_GroupId__c = objAcc.Id);
        insert objCen;
        Census__c objjCenNew = [SELECT Id,vlocity_ins_GroupId__c FROM Census__c];
        system.debug('objjCenNew' +objjCenNew);
        Census_Member__c objCenmem = new Census_Member__c();
        Census_Member__c objCenmem1 = new Census_Member__c();
        objCenmem.Name = 'Silverlinecrm';
        objCenmem.vlocity_ins_IsPrimaryMember__c = FALSE; 
        objCenmem.vlocity_ins_CensusId__c = objjCenNew.Id;
        insert objCenmem;
        objCenmem.vlocity_ins_IsPrimaryMember__c = FALSE; 
        objCenmem1.Name = 'Silverlinecrm1';
        objCenmem1.vlocity_ins_IsPrimaryMember__c = FALSE; 
        objCenmem1.vlocity_ins_CensusId__c = objjCenNew.Id;
        insert objCenmem1;
        objCenmem1.vlocity_ins_IsPrimaryMember__c = FALSE;
        Test.startTest();
        try{
            objCenmem1.vlocity_ins_IsPrimaryMember__c = TRUE;
            update objCenmem1;
        }
        catch(Exception e){
            String message = e.getMessage();
            System.assert(message.contains('Primary memebr is present in the census'),'message=' +message);  
        } 
        Test.stopTest();
    }

}