public class CensusUpdate 
{
    public static void onAfterInsertCensusUpdate(List<Census_Member__c> lstCensusMem) 
    {
        List<Census__c> lstCen = new List<Census__c>();
        for(Census_Member__c objCen : lstCensusMem) 
        {
            if(objCen.vlocity_ins_IsPrimaryMember__c == True) 
            {
                Census__c objCensus = new Census__c();
                objCensus.Id = objCen.vlocity_ins_CensusId__c;
                objCensus.Primary_Member__c = objCen.Name;
                lstCen.add(objCensus);
            }
        }
        if(!lstCen.isEmpty())
        {
            update lstCen;
        }
    }
    
    public static void onAfterUpdateCensusUpdate(Map<Id,Census_Member__c> mapNewMember, Map<Id,Census_Member__c> mapOldMember) 
    {
        List<Census__c> lstCenUpdate = new List<Census__c>();
        for(Id CensId : mapNewMember.keySet())
        {
            Census_Member__c objCensusNew = mapNewMember.get(CensId);
            Census_Member__c objCensusOld = mapOldMember.get(CensId);
            if(objCensusNew.vlocity_ins_IsPrimaryMember__c == True) 
            {
                Census__c objCensu = new Census__c();
                objCensu.Id = objCensusNew.vlocity_ins_CensusId__c;
                objCensu.Primary_Member__c = objCensusNew.Name;
                lstCenUpdate.add(objCensu); 
            }
            if(objCensusNew.vlocity_ins_IsPrimaryMember__c == False) 
            {
                Census__c objCensu = new Census__c();
                objCensu.Id = objCensusNew.vlocity_ins_CensusId__c;
                objCensu.Primary_Member__c = NULL;
                lstCenUpdate.add(objCensu); 
            }
        }
            update lstCenUpdate;
    }
}