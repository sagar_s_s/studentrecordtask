public with sharing class DemoLWCApexController {
    
    // @AuraEnabled(Cacheable=true)
    // public static List<Account> getAccounts(String searchKey){
    //     try {
    //         return [SELECT Id,Name,phone FROM Account WHERE Name =:searchKey LIMIT 10];   
    //     } catch (Exception e) {
    //         throw new AuraHandledException(e.getMessage());
    //     }
    // }
    @AuraEnabled
    public static List<fieldWrapper> getFieldSetMembers(String objectApiName, String fieldSetApiName){
        List<fieldWrapper> lstfieldWrapper = new List<fieldWrapper>();
        Schema.DescribeSObjectResult d = Student__c.sObjectType.getDescribe();

        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
    Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectApiName);
    Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();

    //system.debug('====>' + DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName));

    Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetApiName);

    List<Schema.FieldSetMember> fieldSetMemberList =  fieldSetObj.getFields();
    for(Schema.FieldSetMember objFM : fieldSetMemberList) {
        lstfieldWrapper.add(new fieldWrapper(objFM.getLabel(), objFM.getFieldPath(), objFM.getRequired(), String.valueOf(objFM.getType()), objFM.getDbRequired()));
    }
    //system.debug('fieldSetMemberList ====>' + fieldSetMemberList);  
    return lstfieldWrapper; 

    }

    // @AuraEnabled
    // public static Object createAccount(String strData){
    //     try {
    //         Account objAccount = (Account)JSON.deserialize(strData, Account.class);
    //         if(String.isNotBlank(objAccount.Name))
    //         {
    //             insert objAccount;
    //             return new Map<String,String> {'Id'=>objAccount.Id , 'Name' => objAccount.Name};
    //         }
    //         else{
    //           return 'Account Not Created';  
    //         }
 
    //     } catch (Exception e) {
    //         throw new AuraHandledException(e.getMessage());
    //     }
    // }


    public class fieldWrapper {
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public String apiName {get; set;}
        @AuraEnabled public Boolean isRequired {get; set;}
        @AuraEnabled public String type {get; set;}
        @AuraEnabled public String isDBRequired {get; set;}

        fieldWrapper(String label, String apiName, Boolean isRequired, String type, Boolean isDBRequried) {
            this.label = label;
            this.apiName = apiName;
            this.isRequired = isRequired;
            this.type = type;
            this.isDBRequired = isDbRequired; 

        }

    }
}