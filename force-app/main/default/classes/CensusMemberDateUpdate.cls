public class CensusMemberDateUpdate {
    public static void onBeforeInsertCensusMemberDateUpdate(List<Census_Member__c> lstCensusMem) {
        Map<Id, Census__c> mapCensus = new Map<Id, Census__c>([SELECT vlocity_ins_EffectiveStartDate__c, vlocity_ins_EffectiveEndDate__c FROM Census__c]);
        for( Census_Member__c objCensusMem : lstCensusMem)
        {
            if(objCensusMem.htcs_EffectiveDate__c <  mapCensus.get(objCensusMem.vlocity_ins_CensusId__c).vlocity_ins_EffectiveStartDate__c || objCensusMem.htcs_TermDate__c > mapCensus.get(objCensusMem.vlocity_ins_CensusId__c).vlocity_ins_EffectiveEndDate__c)
            {
                objCensusMem.addError('Invalid Dates,Plz enter correct dates');
            }
        }
        
    }
    
    public static void onBeforeUpdateCensusMemberDateUpdate(Map<Id,Census_Member__c> mapNewMember, Map<Id,Census_Member__c> mapOldMember)
    {
        Map<Id, Census__c> mapCensus = new Map<Id, Census__c>([SELECT vlocity_ins_EffectiveStartDate__c, vlocity_ins_EffectiveEndDate__c FROM Census__c]);
        for(Id CensId : mapNewMember.keySet()) 
        {
            if(mapNewMember.get(CensId).htcs_EffectiveDate__c != NULL || mapNewMember.get(CensId).htcs_TermDate__c !=NULL)
            {
                if(mapNewMember.get(CensId).htcs_EffectiveDate__c <  mapCensus.get(mapNewMember.get(CensId).vlocity_ins_CensusId__c).vlocity_ins_EffectiveStartDate__c || mapNewMember.get(CensId).htcs_TermDate__c > mapCensus.get(mapNewMember.get(CensId).vlocity_ins_CensusId__c).vlocity_ins_EffectiveEndDate__c) 
                {
                    mapNewMember.get(CensId).addError('Check the dates,Enter it in correct way');
                }  
            }
            
        }
        
    }

}