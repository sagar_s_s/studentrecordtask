trigger JewelryTrigger on Jewelry__c (before insert,before update) 
{
    if(Trigger.isBefore && Trigger.isInsert) 
    {
        JewelryHandler.onBeforeInsert(Trigger.New);
    }
    
    if(Trigger.isBefore && Trigger.isUpdate) 
    {
        JewelryHandler.onBeforeUpdate(Trigger.New,Trigger.oldmap);
    }
}