import { LightningElement,track } from 'lwc';
//import getAccounts from '@salesforce/apex/DemoLWCApexController.getAccounts';
//import insertAccount from '@salesforce/apex/DemoLWCApexController.createAccount';
import getFieldSetMembers from '@salesforce/apex/DemoLWCApexController.getFieldSetMembers';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

export default class DemoApex2 extends NavigationMixin(LightningElement) {


   
    @track objAccount = {};
    @track accountId; 
    @track listOfFields;

    connectedCallback() {
        this.fieldMemberHandler();
    }
    fieldMemberHandler(){
        getFieldSetMembers({objectApiName :'Student__c' ,fieldSetApiName : 'studentFieldsForLWC'})
        .then(result =>{
            console.log(result);
            this.listOfFields = result;
            // this.generatedUrlHandler();
            // this.accountId = result.Id;
            // console.log("id" +this.accountId); 
        }).catch(error =>{
            //this.showToastNotification('ERROR',error.body.message, 'error');
            console.error(error);
        })
    }

 
    insertAccountHandler()
    {
        insertAccount({strData : JSON.stringify(this.objAccount)})
        .then(result =>{
            this.showToastNotification('success',`Account '${result.Name}' created successfully`, 'success');
            // this.navigateHandler(result.Id);
            this.generatedUrlHandler();
            this.accountId = result.Id;
            console.log("id" +this.accountId); 
        }).catch(error =>{
            this.showToastNotification('ERROR',error.body.message, 'error');
            console.error(error);
        })
    }

    showToastNotification(title,message,type) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: type,
        });
        this.dispatchEvent(evt);
    }

    // navigateHandler(recordId){
    //     this[NavigationMixin.Navigate]({
    //         type : "standard__recordPage",
    //         attributes: {
    //             //recordId: recordId,
    //             objectApiName: 'Account',
    //             actionName: 'list'
    //         },
    //         state: {
    //             filterName: "AllAccounts"
    //         }
    //     })
    // }

    generatedUrlHandler(){
        let urlPageReference = {
            type : "standard__objectPage",
            attributes: {
                //recordId: recordId,
                objectApiName: 'Account',
                actionName: 'list'
            },
            state: {
                filterName: "AllAccounts"
            }
        }
        this[NavigationMixin.GenerateUrl](urlPageReference).then(result => {this.generatedUrl = result});
    } 

    createRecord()
    {
        this.insertAccountHandler();
    }
 
    inputChangeHandler(event)
    {
        let fieldName = event.target.name;
        console.log("Name: "+fieldName);
        let value = event.target.value;
        console.log("Value: "+value);
        this.objAccount[fieldName] = value;
        console.log("Final Value: "+this.objAccount[fieldName]);
    }

    addressHandler(event)
    {
        let addressName = event.target.name;
        console.log('Address Name: '+addressName );

        if( addressName == "BillingAddress")
        {
            this.objAccount['BillingStreet'] = event.target.street;
            console.log("Street = " +this.objAccount['BillingStreet']);

            this.objAccount['BillingCity'] = event.target.city;
            console.log("City = " +this.objAccount['BillingCity']);

            this.objAccount['BillingState'] = event.target.province;
            console.log("State = " +this.objAccount['BillingState']);

            this.objAccount['BillingCountry'] = event.target.country;
            console.log("Country = " +this.objAccount['BillingCountry']);

            this.objAccount['BillingPostalCode'] = event.target.postalCode;
            console.log("Code = " +this.objAccount['BillingPostalCode']);
        }
        else
        {
            this.objAccount['ShippingStreet'] = event.target.street;
            console.log("Street = " +this.objAccount['ShippingStreet']);

            this.objAccount['ShippingCity'] = event.target.city;
            console.log("City = " +this.objAccount['ShippingCity']);

            this.objAccount['ShippingState'] = event.target.province;
            console.log("State = " +this.objAccount['ShippingState']);

            this.objAccount['ShippingCountry'] = event.target.country;
            console.log("Country = " + this.objAccount['ShippingCountry']);

            this.objAccount['ShippingPostalCode'] = event.target.postalCode;
            console.log("Code = " +this.objAccount['ShippingPostalCode']);
        }
        console.log( JSON.parse(JSON.stringify(this.objAccount)));
    }
}


    // get responseReceived(){
    //     return this.accountList ? true : false;
    // }
    //    @track accountList;
    //    @track searchKey;
   
   
       // @wire(getAccounts,{searchKey : "$searchKey"})
       // accountHandler({data,error}){
       //     if(data) {
       //         console.log(JSON.parse(JSON.stringify(data)));
       //         this.accountList = data;
       //     }
       //     if(error) {
       //         console.log(error);
       //     }
       // }
   
    //        getAccountHandler(){
    //            getAccounts({searchKey : this.searchKey})
    //            .then(result => {
    //                this.accountList = result;
    //            }).catch(error=> {
    //                console.error(error);
    //            })
    //        }
   
    //    searchKeyHandler(event) {
    //        this.searchKey = event.target.value;
    //    }
   
    //    searchRecordHandler(){
    //        this.getAccountHandler();
    //    }