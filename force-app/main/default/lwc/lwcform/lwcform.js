import { LightningElement ,track} from 'lwc';

export default class Form extends LightningElement {
    @track firstName;
    @track lastName;
    @track gender;
    @track designation;
    @track fulltime;
    @track salary;
    @track email;
    @track description;

    @track submit;
    


    onFirstNameHandle(event)
    {
        this.firstName = event.target.value;
        console.log(this.firstName);
    }
    onLastNameHandle(event)
    {
        this.lastName = event.target.value;
    }
    onGenderHandle(event)
    {
        this.gender = event.target.value;
        console.log( this.gender);
    }
    onDesignationHandle(event)
    {
        this.designation = event.target.value;
    }
    onFullTimeHandle(event)
    {
        var full_time  = event.target.checked;
        console.log( full_time);
        if( full_time == true )
        {
            this.fulltime = "Yes";
        }
        else 
        {
            this.fulltime = "No";
        }
        
        console.log( this.fulltime);
    }
    onSalaryHandle(event)
    {
        this.salary = event.target.value;
    }
    onEmailHandle(event)
    {
        this.email = event.target.value;
    }
    onDescriptionHandle(event)
    {
        this.description = event.target.value;
    }
    
    validate()
    {
        if( this.firstName && this.lastName  && this.gender && this.designation  && this.salary && this.email && this.description)
        {
          this.submit =  true;  
        }
        else{
            alert('Enter All Values');
            this.submit = false;
        }
    }
    
}