import { LightningElement,track} from 'lwc';

export default class TableTaskk extends LightningElement {

    value = '';
    get options() {
        return [
            { label: 'Male', value: 'Male' },
            { label: 'Female', value: 'Female' },
        ];
    }
    @track keyIndex = 0;
    @track dataList = [{
        name: '',
        gender: '',
        designation: '',
        fulltime: '',
        salary: '',
        email: '',
        description: ''
    }];
    addRow() {
        this.keyIndex + 1;
        this.dataList.push({
            name: '',
            gender: '',
            designation: '',
            fulltime: '',
            salary: '',
            email: '',
            desciption: ''
        });
    }
    changeHandler(event) {
        if (event.target.name == 'name') {
            this.dataList[event.target.accesKey].name = event.target.value;
        } else if (event.target.name == 'salary') {
            this.dataList[event.target.accesKey].salary = event.target.value;
        } else if (event.target.name == 'email') {
            this.dataList[event.target.accesKey].email = event.target.value;
        } else if (event.target.name == 'description') {
            this.dataList[event.target.accesKey].description = event.target.value;
        } else if (event.target.name == 'designation') {
            this.dataList[event.target.accesKey].designation = event.target.value;
        } else if (event.target.name == 'gender') {
            this.dataList[event.target.accesKey].gender = event.target.value;
        } else if (event.target.name == 'fulltime') {
            this.dataList[event.target.accesKey].fulltime = event.target.value;
        }
    }
    removeRow(event) {
        if (this.dataList.length >= 1) {
            this.dataList.splice(event.target.accesKey, 1);
            this.keyIndex - 1;
        }
    }
}