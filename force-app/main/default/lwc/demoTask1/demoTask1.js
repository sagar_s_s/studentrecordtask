import { LightningElement,track } from 'lwc';

export default class DemoTask1 extends LightningElement {

           @track firstName;
           @track lastName;
           @track gender;
           @track submit;
           @track designation;
           @track fulltime;
           @track salary;
           @track email;
           @track description;


         firstNameHandle(event) {
               this.firstName = event.target.value;
           }

           lastNameHandle(event) {
            this.lastName = event.target.value;
        }

        genderHandle(event) {
            this.gender = event.target.value;
        }

        designationHandle(event) {
            this.designation = event.target.value;
        }

        fulltimeHandle(event) {
            this.fulltime = event.target.checked;
        }

        salaryHandle(event) {
            this.salary = event.target.value;
        }

        emailHandle(event) {
            this.email = event.target.value;
        }

        descriptionHandle(event) {
            this.description = event.target.value;
        }

        submitHandle(){
            if(this.firstName && this.lastName && this.gender && this.designation  && this.salary && this.email && this.description) {
                this.submit = true;
            }
             else {
                 alert("Enter All The Values")
                this.submit = false;
            }
        }
}