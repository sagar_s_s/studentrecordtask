import { LightningElement,track,api } from 'lwc';

export default class ParentComponent extends LightningElement {
    @api recordId;
    @api objectApiName;
    @track listContacts = [

        {Name: "Silverline1", phone: 9945203876, isActive: false},
        {Name: "Silverline2", phone: 9945203873, isActive: true},
        {Name: "Silverline3", phone: 9945203872, isActive: true},
        {Name: "Silverline4", phone: 9945203871, isActive: false}
    ]

    @track inputValue;

    valuechangeHandler(event) {
        this.inputValue = event.detail;
    }
}