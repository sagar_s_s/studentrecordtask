import { api, LightningElement, track } from 'lwc';

export default class ChildComponent extends LightningElement {

    @api objContact;
    @track updateValue;


    get appliedClass() {
        return this.objContact.isActive ? "slds-text-color_success" : "slds-text-color_error"
    }

    inputHandler(event) {
        this.updateValue = event.target.value;
        let valueChangeEvent = new CustomEvent("valuechange",{detail : this.updateValue});
        this.dispatchEvent(valueChangeEvent);


    }
}