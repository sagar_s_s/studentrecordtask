import { LightningElement,track} from 'lwc';
import getAccounts from '@salesforce/apex/DemoLWCApexController.getAccounts';

export default class DemoApex1 extends LightningElement {

 get responseReceived(){
     return this.accountList ? true : false;
 }
    @track accountList;
    @track searchKey;


    // @wire(getAccounts,{searchKey : "$searchKey"})
    // accountHandler({data,error}){
    //     if(data) {
    //         console.log(JSON.parse(JSON.stringify(data)));
    //         this.accountList = data;
    //     }
    //     if(error) {
    //         console.log(error);
    //     }
    // }

        getAccountHandler(){
            getAccounts({searchKey : this.searchKey})
            .then(result => {
                this.accountList = result;
            }).catch(error=> {
                console.error(error);
            })
        }

    searchKeyHandler(event) {
        this.searchKey = event.target.value;
    }

    searchRecordHandler(){
        this.getAccountHandler();
    }
}