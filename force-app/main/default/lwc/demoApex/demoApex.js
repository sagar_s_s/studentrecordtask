import { LightningElement,wire,track} from 'lwc';
import getAccounts from '@salesforce/apex/DemoLWCApexController.getAccounts';

export default class DemoApex extends LightningElement {

// @wire(getAccounts)
// accounts;

 get responseReceived(){
     return this.accountList ? true : false;
 }
    @track accountList;
    @track searchKey;


    @wire(getAccounts,{searchKey : "$searchKey"})
    accountHandler({data,error}){
        if(data) {
            console.log(JSON.parse(JSON.stringify(data)));
            this.accountList = data;
        }
        if(error) {
            console.error(error);
        }
    }

    searchKeyHandler(event) {
        this.searchKey = event.target.value;
    }
}