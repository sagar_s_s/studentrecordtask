import { LightningElement,track,api} from 'lwc';

import getFieldSetMembers from '@salesforce/apex/studentRecordHandler.getFieldSetMembers';
import createAccount from '@salesforce/apex/studentRecordHandler.createAccount';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import getDetails from '@salesforce/apex/studentRecordHandler.getDetails';

export default class StudentRecord extends NavigationMixin (LightningElement)
 {
    @api recordId;
    @track objStudent = {};
    // @track studentId;
    @track listOfFields;
    //@track listStudent;
    @track cancelStatus = true;


//////////////////////////////////////////////// HOOKs ////////////////////////////////////////

    connectedCallback() {
        this.fieldMemberHandler();
    }

    renderedCallback() {
        this.recordFetch();
    }

///////////////////////////////////////////FieldsetsHandler////////////////////////////////////

    fieldMemberHandler(){
        getFieldSetMembers({objectApiName :'Student__c' ,fieldSetApiName : 'studentFieldsForLWC'})
        .then(result =>{
            console.log(result);
            this.listOfFields = result;
            for(var i=0;i<result.length;i++){
                if(result[i].type == "BOOLEAN")
                {
                    result[i].type = "checkbox";
                }
                else if(result[i].type == "DOUBLE")
                {
                    result[i].type = "number";
                }
            }
        }).catch(error =>{
            console.error(error);
        })
    }

///////////////////////////////////Insert a Record//////////////////////////////////////////////

    insertAccountHandler()
    {
        createAccount({objStudent : this.objStudent})
        .then(result =>{
            if(this.recordId == null && result.Name == null){
                this.showToastNotification('ERROR', `Enter required Fields`, 'error');
            }
            else if(this.recordId == null){
                this.showToastNotification('SUCCESS', `Student Record "${result.Name}" is Created Successfully`, 'success');
                this.navigateHandler(result.Id);
            }
                else if(this.recordId != null) {
                this.showToastNotification('SUCCESS', `Student Record "${result.Name}"  is Updated Successfully`, 'success');
                this.navigateHandler(result.Id);
            }
        }).catch( error =>{
            this.showToastNotification('ERROR',`Record2 is not Created`, 'error');
            console.error(error);
        })
    }

    createRecord()
    {
        this.insertAccountHandler();
    }

////////////////////////////////////Reset the fields///////////////////////////////////////////////////////

resetHandler(){
        if(this.recordId == null)
        {
            this.objStudent = {};
            this.template.querySelectorAll('lightning-input').forEach(element => {
                if (element.type === 'checkbox' || element.type === 'checkbox-button') {
                    element.checked = false;
                } else {
                    element.value = null;
                }
            });
        }
        else if (this.recordId != null)
        {
            console.log("Cancellll");
            this.cancelPageNavigate(this.recordId);
        }
    }

    cancelPageNavigate(recordId) {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: recordId,
                objectApiName: 'Student__c',
                actionName: 'view'
            }
        });
    }

///////////////////////////////////To show a Success Message//////////////////////////////////////////////

    showToastNotification(title,message,type) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: type,
        });
        this.dispatchEvent(evt);
    }

///////////////////////////////////Navigate to Recordpage//////////////////////////////////////////////

    navigateHandler(recordId){
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: recordId,
                objectApiName: 'Student__c',
                actionName: 'view'
            }
        });
    }

////////////////////////////////////////////InputHandler for Fields//////////////////////////

    inputChangeHandler(event)
    {
        let fieldName = event.target.name;
        console.log("Name: "+fieldName);
        let value = event.target.value;
        console.log("Value: "+value);

        if(this.recordId !=null){
            this.objStudent['Id'] = this.recordId;
        }

        if(event.target.type == "checkbox"){
            this.objStudent[fieldName] = event.target.checked;
        }else 
        {
            this.objStudent[fieldName] = value;
            console.log("Final Value: "+this.objStudent[fieldName]);
        }
    }

/////////////////////////////////////////////Fetching the Record///////////////////////////////

    recordFetch()
    {
        getDetails({recordId : this.recordId}).then(result => {
            for(var i= 0; this.listOfFields.length; i++)
            {
                for (const [key, value] of Object.entries(result[0])) 
                {
                    if( this.listOfFields[i].apiName == key)
                    {
                        this.listOfFields[i].val = value;
                    }
                }
            }
        }) .catch (error =>{
            console.error(error);
        })
    }

    editHandler()
    {
        this.cancelStatus = false;
    }
 }
 ///////////////////////////////////////////- END -////////////////////////////////////////////