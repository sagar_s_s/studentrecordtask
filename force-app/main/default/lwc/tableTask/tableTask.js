import { LightningElement, track } from 'lwc';

export default class TableTask extends LightningElement {

    keyIndex = 0;
    @track listOfRecords = [];
    get options() {
        return [
            { label: 'Male', value: 'Male' },
            { label: 'Female', value: 'Female' },
        ];
    }

    /////////////////////////////Add a Row//////////////////////////////////////////////////////////////////////
    addRowHandler(event) {
        ++this.keyIndex;
        var saveItem = [{ id: this.keyIndex, Name: '', Gender: '', Designation: '', FullTime: '', Salary: '', Email: '', Description: '', edit: true }];
        this.listOfRecords = this.listOfRecords.concat(saveItem);
        let rowList = JSON.parse(JSON.stringify(this.listOfRecords))
        console.log(rowList);
        var indx = this.listOfRecords.length;
        let indexx = event.target.dataset.value;
    }



    ////////////////////////////////////Save a Row//////////////////////////////////////////////////

    @track name;
    @track gender;
    @track designation;
    @track fulltime;
    @track salary;
    @track email;
    @track description;
    @track submit;

    onNameHandle(event) {
        let index = event.target.dataset.id;
        console.log(index);
        this.listOfRecords.forEach(ele => {
            if (ele.id == index) { ele.Name = event.target.value; }
        })
        // this.name = event.target.value;
        // console.log(this.name);
    }
    onGenderHandle(event) {
        let index = event.target.dataset.id;
        console.log(index);
        this.listOfRecords.forEach(ele => {
            if (ele.id == index) { ele.Gender = event.target.value; }
        })
        //this.gender = event.target.value;
        //console.log( this.gender);
    }
    onDesignationHandle(event) {
        let index = event.target.dataset.id;
        console.log(index);
        this.listOfRecords.forEach(ele => {
            if (ele.id == index) { ele.Designation = event.target.value; }
        })
        // this.designation = event.target.value;
        // console.log(this.designation);
    }
    onFullTimeHandle(event) {
        let index = event.target.dataset.id;
        console.log(index);
        this.listOfRecords.forEach(ele => {
            if (ele.id == index) {
                if (event.target.checked == true) {
                    ele.fulltime = event.target.checked;
                }
                else {
                    ele.fulltime = event.target.checked;
                }
            }
        })
    }
    onSalaryHandle(event) {
        let index = event.target.dataset.id;
        console.log(index);
        this.listOfRecords.forEach(ele => {
            if (ele.id == index) { ele.Salary = event.target.value; }
        })
        //this.salary = event.target.value;
        // console.log(this.salary);

    }
    onEmailHandle(event) {
        let index = event.target.dataset.id;
        console.log(index);
        this.listOfRecords.forEach(ele => {
            if (ele.id == index) { ele.Email = event.target.value; }
        })
        //this.email = event.target.value;
        // console.log(this.email);

    }
    onDescriptionHandle(event) {
        let index = event.target.dataset.id;
        console.log(index);
        this.listOfRecords.forEach(ele => {
            if (ele.id == index) { ele.Description = event.target.value; }
        })
        //this.description = event.target.value;
        // console.log(this.description);
    }

    saveHandler(event) {

        let index = event.target.accessKey;
        this.listOfRecords.forEach(ele => {
            if (ele.id == index) { ele.edit = false; }
        })
    }


    /////////////////////////////////////////Delete a Row//////////////////////////////////////////////////
    removeHandler(event) {
        console.log("Calling delete");
        console.log("length=" + this.listOfRecords.length);
        console.log("Target.accesskey=" + parseInt(event.target.accessKey));
        if (this.listOfRecords.length >= 1) {

            this.listOfRecords = this.listOfRecords.filter(function (element) {

                return parseInt(element.id) !== parseInt(event.target.accessKey);
            });
        }
    }


    ////////////////////////////////////////Edit a Row//////////////////////////////////////////////////////
    editRowHandler(event) {

        var selectedRow = event.target.accessKey;
        for (let i = 0; i < this.listOfRecords.length; i++) {
            if (this.listOfRecords[i].id == selectedRow) {
                this.listOfRecords[i].edit = true;
                this.listOfRecords[i].Name.value = event.target.value;
                this.listOfRecords[i].Gender.value = event.target.value;
                this.listOfRecords[i].Designation.value = event.target.value;
                this.listOfRecords[i].FullTime.value = event.target.checked;
                this.listOfRecords[i].Salary.value = event.target.value;
                this.listOfRecords[i].Email.value = event.target.value;
                this.listOfRecords[i].Description.value = event.target.value;


            }
        }

    }
}
// changeHandler(event) {
//     if (event.target.name == 'name') {
//         this.dataList[event.target.accesKey].name = event.target.value;
//     } else if (event.target.name == 'salary') {
//         this.dataList[event.target.accesKey].salary = event.target.value;
//     } else if (event.target.name == 'email') {
//         this.dataList[event.target.accesKey].email = event.target.value;
//     } else if (event.target.name == 'description') {
//         this.dataList[event.target.accesKey].description = event.target.value;
//     } else if (event.target.name == 'designation') {
//         this.dataList[event.target.accesKey].designation = event.target.value;
//     } else if (event.target.name == 'gender') {
//         this.dataList[event.target.accesKey].gender = event.target.value;
//     } else if (event.target.name == 'fulltime') {
//         this.dataList[event.target.accesKey].fulltime = event.target.value;
//     }
// }